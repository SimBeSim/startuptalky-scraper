from urllib.parse import urlparse, urlunparse

from bs4 import BeautifulSoup
import requests

from .models import Headers


class Scraper():
    sitemap = 'sitemap.xml'

    def __init__(self, site):
        self.site = site
        self.req_session = requests.Session()
        self.req_session.verify = False

    def scrape_h2s(self, loc):
        try:
            page = self.req_session.get(loc)

            page = BeautifulSoup(page.content, 'html.parser')

            h2s = page.find_all('h2')

            return h2s
        except Exception:
            return []

    def scrape_sitemap(self, sitemap_loc):
        try:
            sitemap = self.req_session.get(sitemap_loc)

            sitemap = BeautifulSoup(sitemap.content, 'xml')

            return sitemap
        except Exception:
            return ''

    def scrape(self):
        site = list(urlparse(self.site))
        site[2] = self.sitemap
        site = urlunparse(site)

        sitemap_index = self.scrape_sitemap(site)

        sitemaps = sitemap_index.find_all('loc')

        for sm in sitemaps[:2]:
            h2s = []

            urlset = self.req_session.get(sm.get_text())

            urlset = BeautifulSoup(urlset.content, 'xml')

            urls = urlset.find_all('url')

            for url in urls[:5]:
                loc = url.loc.get_text()

                headers = self.scrape_h2s(loc)

                h2s.append({
                    'page': loc,
                    'h2s': [h.get_text() for h in headers],
                    'lastModified': url.lastmod.get_text()
                })

            try:
                Headers.objects.create(
                    site=self.site,
                    h2s=h2s
                )
            except Exception as exc:
                print(exc)


def scrape_all():
    pass


def scrape_one(site):
    target = Scraper(site)
    target.scrape()
