from django.shortcuts import render, redirect
from django.contrib import messages
from django.views.generic import (
    ListView
)

from .tasks import scrape_now
from .models import Headers
from .forms import NewSite


def home(request):
    return render(request, 'scraper/home.html')


def scrape(request):
    form = NewSite(request.POST)

    if form.is_valid():
        site = form.cleaned_data.get('site')
        scrape_now.delay(site)
    else:
        messages.error(request, 'Invalid form input')
        return redirect('/scraper')

    messages.info(request, 'Site has been queued. Results will be available after some time')
    return redirect('/scraper')


class SitesListView(ListView):
    model = Headers
    template_name = 'scraper/scraped.html'
    context_object_name = 'sites'
    paginate_by = 5

    def get_queryset(self, **kwargs):
        return Headers.objects \
                .all() \
                .order_by('date_scraped').only('site', 'date_scraped')


class HeadersListView(ListView):
    model = Headers
    template_name = 'scraper/h2s.html'
    context_object_name = 'h2s'
    paginate_by = 20

    def get_queryset(self, **kwargs):
        return Headers.objects.filter(site=kwargs.get('site', '')).order_by('date_scraped')
